// Libraries
import React from 'react';
import { IntlProvider } from 'react-intl';

// Components
import Views from './views';

//Assets
import locales from './assets/locales';
import './App.scss';

const CURRENT_LOCALE = "en";
const messages = locales[CURRENT_LOCALE];

class App extends React.Component {
  componentDidMount() {
    document.title = messages['app.title'];
  }

  render() {
    return (
      <IntlProvider locale={ CURRENT_LOCALE } messages={ messages}>
        <div className="app">
          <Views />
        </div>
      </IntlProvider>
    );
  }
}

export default App;
