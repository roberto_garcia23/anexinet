export const areValidArrays = (arrays = []) => {
    for(const array of arrays) {
        if (
            !Array.isArray(array) ||
            array.length === 0
        ) {
            return false;
        }
    };

    return true; 
};

export const sendAlert = (message) => {
    alert(message);
};

const isInt = (number) => {
    return Number(number) === number && number % 1 === 0;
};

const isFloat = (number) => {
    return Number(number) === number && number % 1 !== 0;
};

export const areValidnumber = (number) => {
    return (
        (
            isInt(number) ||
            isFloat(number)
         ) &&
        (number >= 0 && number <= 1)
    );
};

export const dec2Bin = (number) => {
    return Number(number).toString(2);
};
