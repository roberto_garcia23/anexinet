// Libraries
import React from 'react';
import ReactDOM from 'react-dom';

// Components
import App from './App';

// Assets
import './index.scss';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
