// Libraries
import React from "react";
import Proptypes from "prop-types";
import { injectIntl } from "react-intl";
import { areValidArrays, sendAlert } from "../../libs/general";

//Assets
import "./styles.scss";

class ExerciseOne extends React.Component {
  static propTypes = {
    intl: Proptypes.object
  }

  compareTwoArrays = (arrayOne = [], arrayTwo = []) => {
    if (!areValidArrays([arrayOne, arrayTwo])) {
      const {
        intl
      } = this.props;

      sendAlert(intl.formatMessage({ id: "app.alert.error" }));
      return [];
    }

    const matchesValues = arrayOne.filter(value => arrayTwo.includes(value));

    return matchesValues;
  }

  render() {
    const {
      intl
    } = this.props;

    const comparisonResult = this.compareTwoArrays(["cat", "dog", "rabbit"], ["kangaroo", "rabbit", "cat"]);
    
    return (
      <div className="content-one">
        { comparisonResult.length > 0 &&
          <>
            <p>
              {
                intl.formatMessage({ id: "app.exercise.one.views.title" })
              }
            </p>
            <ul>
              {
                comparisonResult.map((value, key) => <li key={ key }>{ value }</li>)
              }
            </ul>
          </>
        }
      </div>
    );
  }
}

export default injectIntl(ExerciseOne);
