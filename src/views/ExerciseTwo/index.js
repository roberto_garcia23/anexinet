// Libraries
import React from "react";
import Proptypes from "prop-types";
import { injectIntl } from "react-intl";
import { areValidArrays, sendAlert } from "../../libs/general";

//Assets
import "./styles.scss";

class ExerciseTwo extends React.Component {
  static propTypes = {
    intl: Proptypes.object
  }

  getListTemplate = (array = []) => {
    if (!areValidArrays([array])) {
      const {
        intl
      } = this.props;

      sendAlert(intl.formatMessage({ id: "app.alert.error" }));
      return undefined;
    }

    return (
      <div className="template">
        {
          array.map((value, index) => <div key={ index } className="element">{ value }</div>)
        }
      </div>
    );
  }

  render() {
    const {
      intl
    } = this.props;

    const listTemplate = this.getListTemplate(["November", "is", "the", "coolest", "month", "of", "the", "Year"]);
    return (
      <div className="content">
        {
          listTemplate &&
          <>
            <p>
              {
                intl.formatMessage({ id: "app.exercise.two.views.title" })
              }
            </p>
            {
              listTemplate
            }
          </>
        }
      </div>
    );
  }
}

export default injectIntl(ExerciseTwo);
