// Libraries
import React from "react";
import Proptypes from "prop-types";
import { injectIntl } from "react-intl";
import { areValidnumber, dec2Bin, sendAlert } from "../../libs/general";

//Assets
import "./styles.scss";

class ExerciseSeven extends React.Component {
  static propTypes = {
    intl: Proptypes.object
  }

  getBinaryRepresentation = (number = -1) => {
    if (!areValidnumber(number) || dec2Bin(number).length >= 34) {
      const {
        intl
      } = this.props;
      
      sendAlert(intl.formatMessage({ id: "app.alert.error" }));
      return undefined;
    } 

    return number;
  }

  render() {
    const {
      intl
    } = this.props;

    const binaryRepresentation = this.getBinaryRepresentation(0.15);
    return (
      <div className="content-seven">
        {
          binaryRepresentation &&
          <>
            <p>
              {
                intl.formatMessage({ id: "app.exercise.seven.views.title" })
              }
            </p>
            {
              binaryRepresentation
            }
          </>
        }
      </div>
    );
  }
}

export default injectIntl(ExerciseSeven);
