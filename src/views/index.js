// Libraries
import React from "react";
import Proptypes from "prop-types";
import { injectIntl } from "react-intl";

// Components
import Header from "../components/layout/Header";
import ExerciseOne from "./ExerciseOne";
import ExerciseTwo from "./ExerciseTwo";
import ExerciseSeven from "./ExerciseSeven";

//Assets
import "./styles.scss";

class Views extends React.Component {
  static propTypes = {
    intl: Proptypes.object
  }

  state = {
    numberOfExercise: 1
  }

  goToExercise = (numberOfExercise) => () => {
    this.setState({ numberOfExercise })
  }

  getNamingExercise = () => {
    const {
      numberOfExercise
    } = this.state;

    return numberOfExercise === 1 ? "one" : numberOfExercise === 2 ? "two" : "seven";
  }

  getTemplate = () => {
    const {
      numberOfExercise
    } = this.state;

    return numberOfExercise === 1 ? <ExerciseOne /> : numberOfExercise === 2 ? <ExerciseTwo /> : <ExerciseSeven />;
  }

  render() {
    const {
      intl
    } = this.props;
    
    return (
        <>
            <Header
              number={ intl.formatMessage({ id: `app.exercise.${this.getNamingExercise()}` }) }
              description={ intl.formatMessage({ id: `app.exercise.${this.getNamingExercise()}.description` }) }
            />
            <div className="content-views">
              <button onClick={ this.goToExercise(1) }>
                First exercise
              </button>
              <button onClick={ this.goToExercise(2) }>
                Second exercise
              </button>
              <button onClick={ this.goToExercise(7) }>
                Seven exercise
              </button>
              {
                this.getTemplate()
              }
            </div>
        </>
    );
  }
}

export default injectIntl(Views);
