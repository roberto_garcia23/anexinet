// Libraries
import React from "react";
import Proptypes from "prop-types";

//Assets
import "./styles.scss";

class Header extends React.Component {
  static propTypes = {
    number: Proptypes.string.isRequired,
    description: Proptypes.string.isRequired
  }

  render() {
    const {
      number,
      description
    } = this.props;
    
    return (
      <div className="header">
        <p>
          { number }
        </p>
        <p>
          { description }
        </p>
      </div>
    );
  }
}

export default Header;
